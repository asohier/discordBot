#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from pickle import dump, load
from random import randint
from re import findall
from time import time

import util.cfg

##### Parameters #####
util.cfg.default = {"chainWindowSize":5, "randomWalk": False, "wordDicFilename": "wordDic.pickle", "chainDicFilename": "chainDic.pickle", "forgetTime": 300.0, "talkability": 50, "randomness": 10, "trimDelay": 24.0*3600.0}

##### Internal variables #####
wordDic = {}
chainDic = {}
previous = ""
previousTS = 0
lastTrimTS = 0

##### Main Functions #####
def wordToNum(word):
    """Get the correspondance word -> index in dictionnary, and adds the word if not present"""
    global wordDic
    if not wordDic.__contains__(word):
        wordDic[word] = len(wordDic) + 1
    return wordDic[word]

def numToWord(num):
    """Get the correspondance word <- index from dictionary"""
    return list(wordDic.keys())[list(wordDic.values()).index(num)]

def wordList(sentence):
    """Turns a sentence in a list of words and punctuaction groups (eg smileys etc)"""
    return (" ".join([i.lower()+" "+j for i, j in findall("([\w]*)([\W]*)", sentence)])).split()

def chainFromWords(words):
    """Creates a chain of successive word indexes (from a sentence) and updates the chain database consequently"""
    w = [0, ] + words + [0, ]
    chain = ["|".join("%d" % j for j in w[i:i+cfg["chainWindowSize"]]) for i in range(max(len(w)-(cfg["chainWindowSize"]-1), 1))]
    if len(chain) == 1:
        chain = [chain[0][:-2], chain[0][2:]]
    if previous != "" and time() - previousTS < cfg["forgetTime"]:
        chain = [previous, ] + chain
    updateChainStat(chain)

    return chain

def chainToWords(chain):
    """Creates a sentence from an inputted chain"""
    ch = [int(i) for i  in chain[0].split("|")][1:] + [int(item.split("|")[-1]) for item in chain[1:]]
    return " ".join([numToWord(i) for i in ch])

def walkRandomChain():
    """Using the previous last chain item if available will choose a start chain (or a random start chain if not) to walk to a complete sentence chain """
    startChoice={}
    if previous != "" and chainDic.__contains__(previous):
        startChoice = chainDic[previous]
    elif cfg["randomness"] >= randint(0, 100):
        startChoice = {item:1 for item in chainDic.keys() if item[0:2] == "0|"}
    chain = []
    if len(startChoice) > 0:
        while True:
            if cfg["randomWalk"]:
                startChainItem = startChoice.keys()[randint(0, len(startChoice)-1)]
            else:
                choices = []
                for item in startChoice.keys():
                    choices += [item, ] * startChoice[item]
                startChainItem = choices[randint(0, sum(startChoice.values())-1)]
            if startChainItem[-2:] == "|0":
                return chain
            chain += [startChainItem, ]
            startChoice = chainDic[startChainItem]
    return chain

def analyzeSentence(sentence):
    """Completely analyze an inputted sentence, add the missing word to the dictionnary, creates the chains, updates the chains statistics."""
    global previous, previousTS
    words = [wordToNum(word) for word in wordList(sentence)]
    chain = chainFromWords(words)
    with open(cfg["wordDicFilename"], "wb") as f:
        dump(wordDic, f)
    with open(cfg["chainDicFilename"], "wb") as f:
        dump(chainDic, f)
    previous = chain[-1]
    previousTS = time()
    if previousTS - lastTrimTS > cfg["trimDelay"]:
        trimGraph()

def computeAnswer(sentence):
    global previous, previousTS
    analyzeSentence(sentence)
    chain = walkRandomChain()
    if len(chain) > 0:
        previous = chain[-1]
        previousTS = time()
        updateChainStat(chain)
        return chainToWords(chain)
    return ""

def updateChainStat(chain):
    """Updates chain statistics in chain dictionnary"""
    global chainDic
    for idx, item in enumerate(chain[:-1]):
        if chainDic.__contains__(item):
            if chainDic[item].__contains__(chain[idx+1]):
                chainDic[item][chain[idx+1]] += 1
            else:
                chainDic[item][chain[idx+1]] = 1
        else:
            chainDic[item] = {chain[idx+1]:1}

def trimGraph():
    """Trims the graph."""
    global chainDic, lastTrimTS
    itemsToCheck = chainDic.keys()
    for chainItem in itemsToCheck:
        avgIntWeight = sum(chainDic[chainItem].values()) // len(chainDic[chainItem])
        if avgIntWeight > 1:
            # Some items' weight are > 1, let's down all items' weight
            for nextChainItem in chainDic[chainItem].keys():
                chainDic[chainItem][nextChainItem] //= avgIntWeight
        if sum(chainDic[chainItem].values()) < len(chainDic[chainItem]):
            # Some items' weight are < 1 (ie = 0)
            nextItemsToCheck = chainDic[chainItem].copy()
            for nextChainItem in nextItemsToCheck.keys():
                if chainDic[chainItem][nextChainItem] == 0:
                    del chainDic[chainItem][nextChainItem]
    lastTrimTS = time()

def initDb():
    """Initializes data structures"""
    global wordDic, chainDic, cfg
    cfg = util.cfg.load("cfg/markov.2.cfg")

    try:
        with open(cfg["wordDicFilename"], "rb") as f:
            wordDic = load(f)
        with open(cfg["chainDicFilename"], "rb") as f:
            chainDic = load(f)
    except:
        pass

def saveCfg():
    """Save cfg to disk."""
    util.cfg.save(cfg, "cfg/markov.2.cfg")

def dumpGraph(fileName):
    """Dump graph to dot file"""
    with open(fileName, "w") as f:
        f.write("digraph G {\n")
        for chainItem in chainDic.keys():
            left = " ".join([numToWord(int(wordIndex)) for wordIndex in chainItem.split("|") if wordIndex!="0"]).replace('\\', '\\\\').replace('"', '\\"')
            for rightChainItem in chainDic[chainItem].keys():
                right = " ".join([numToWord(int(rightWordIndex)) for rightWordIndex in rightChainItem.split("|") if rightWordIndex!="0"]).replace('\\', '\\\\').replace('"', '\\"')
                f.write('\t"%s" -> "%s";\n' % (left, right))
        f.write("}")    

def test():
    initDb()
    while True:
        answer = computeAnswer(input("> "))
        while len(answer) > 0:
            print(answer)
            answer = computeAnswer(answer)
