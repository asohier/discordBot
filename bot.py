#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# vim: foldlevel=0

# Copyright (C) 2016, Art SoftWare
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/.
# ********************************************************************
# For any questions, feture request or bug reports please contact me
# at support@art-software.fr

from sys import path
import os

curdir=os.path.dirname(__file__)
if curdir=="":
    curdir=os.getcwd()
curdir=os.path.realpath(curdir)

path.insert(1, curdir)
os.chdir(curdir)

import asyncio
import discord

from util.modules import Modules
import util.cfg

class Bot:
    def __init__(self):
        self.usernameDB = {}
        self.modules = Modules(self)
        self.client = discord.Client()
        self.hooks = {'MESSAGE':[]}
        self.cfg = util.cfg.load("cfg/bot.json")

    def start(self):
        @self.client.event
        async def on_ready():
            print('Logged in as {} ({})'.format(self.client.user.name, self.client.user.id))
            geek = None
            general = None
            server = list(self.client.servers)[0]
            channelIdCache = {}

            def findChannel(name):
                for c in server.channels:
                    if name[0] == "#":
                        name = name[1:]
                    if c.name == name:
                        return c.id

            while True:
                m = self.modules['modules.talk.ircBridge']
                print("Started discord>irc pool")
                while True:
                    message = m.getMessage()
                    if message != None:
                        if not message['channel'] in channelIdCache:
                            channelIdCache[message['channel']] = findChannel(message['channel'])
                        if message['channel'] in channelIdCache:
                            if message['author']==self.cfg["nick"]:
                                await self.client.send_message(server.get_channel(channelIdCache[message['channel']]), '{m}'.format(m=message['message']))
                            else:
                                await self.client.send_message(server.get_channel(channelIdCache[message['channel']]), '<{a}> {m}'.format(a=message['author'], m=message['message']))
                    await asyncio.sleep(0.5)

        @self.client.event
        async def on_message(message):
            for func in self.hooks['MESSAGE']:
                print("{}".format(func.__module__ + "." + func.__name__))
                await func(message)

        with open("credentials.txt", "r") as f:
            login = f.readline().replace("\n", "")
            password = f.readline().replace("\n", "")
        self.modules.loadAll('modules')
        self.client.run(login, password)

    def getUserName(self, uid):
        found = 0
        for server in self.client.servers:
            clients = {c.id:c for c in server.members}

            if uid in clients:
                found += 1
                if clients[uid].name not in ["", " ", " "]:
                    self.usernameDB[uid] = clients[uid].name
                    return clients[uid].name
                break
        if found == 0 and uid in self.usernameDB:
            return self.usernameDB[uid]
        return ""

    async def send(self, target, message):
        self.modules['modules.talk.ircBridge'].poolAdd(target, self.cfg["nick"], message)
        await self.client.send_message(target, message)

while True:
    try:
        instance = Bot()
        instance.start()
    except KeyboardInterrupt:
        exit(0)
    except RuntimeError:
        print("Discord.py died, relaunching…")
