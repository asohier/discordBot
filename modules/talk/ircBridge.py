#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Regular expression triggers on user messages."""

from os import unlink, listdir, mkdir
from os.path import exists
from time import time
import re

bot = None

spoolBaseDir='/tmp/spool'
spoolDir='/tmp/spool/dsc'

def init(botInstance):
    """Inits the msgTrigger module"""
    global bot, triggers

    bot = botInstance
    bot.modules.check("modules.main.hooks")
    bot.modules["modules.main.hooks"].addHook('MESSAGE', msgHook)
    
    if not exists(spoolDir+"/in"):
        if not exists(spoolDir):
            if not exists(spoolBaseDir):
                mkdir(spoolBaseDir, mode=int('1750', 8))
            mkdir(spoolDir, mode=int('1750', 8))
        mkdir(spoolDir+"/in", mode=int('1750', 8))
    if not exists(spoolDir+"/out"):
        mkdir(spoolDir+"/out", mode=int('1750', 8))

def getMessage():
    l=[]
    chan = ""
    for chan in listdir('{s}/in'.format(s=spoolDir)):
        l = sorted(listdir('{s}/in/{c}'.format(s=spoolDir, c=chan)))
        if len(l)>0:
            break
    if len(l)==0:
        return None

    spoolMessageFileName = l[0]
    ts, author = spoolMessageFileName.split(":", 1)
    channel = chan
    with open('{s}/in/{c}/{f}'.format(s=spoolDir, c=channel, f=spoolMessageFileName), 'r') as f:
        message = f.read()
    unlink('{s}/in/{c}/{f}'.format(s=spoolDir, c=chan, f=spoolMessageFileName))
    if message[0:7] == "\x01ACTION" and message[-1] == "\x01":
        message = "_{}_".format(message[7:-1])
    return {'channel':channel, 'author':author, 'message':message}

def poolAdd(channel, author, message):
    foundIDs = []
    ids = sorted(re.findall('<@!?([0-9]+)>', message))
    for uId in ids:
        if not uId in foundIDs:
            uName = bot.getUserName(uId)
            message = message.replace('<@{}>'.format(uId), uName).replace('<@!{}>'.format(uId), uName)
            foundIDs.append(uId)
    
    outChannelDir='{s}/out/#{c}'.format(s=spoolDir, c=channel)
    if exists(outChannelDir):
        for target in listdir(outChannelDir):
            with open('{s}/{t}:{a}'.format(s=outChannelDir+"/"+target, t=int(time()), a=author.replace("/", "").replace(" ", "_")), "w") as f:
                f.write(message)

async def msgHook(message):
    """Hook for the event MESSAGE"""
    user = message.author
    tgt = message.channel
    txt = message.content
    if user.id != bot.client.user.id:
        for attachment in message.attachments:
            if 'url' in attachment:
                txt += "\n{}".format(attachment["url"])
            elif 'proxy_url' in attachment:
                txt += "\n{}".format(attachment["url"])
        poolAdd(tgt, user.name, txt)
