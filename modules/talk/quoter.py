#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Quoter module"""

bot = None
quoteBD = {}
lastUserPhrase = {}
usernameDB = {}

import util.cfg
from discord import utils
from random import randint

def init(botInstance):
    """Inits the module"""
    global bot, quoteBD

    bot = botInstance
    bot.modules.check("modules.main.simpleCommand")
    bot.modules.check("modules.main.hooks")

    util.cfg.default = quoteBD
    quoteBD = util.cfg.load("cfg/quote.json")

    bot.modules["modules.main.simpleCommand"].registerCommand(cmdQuote, "quote")
    bot.modules["modules.main.simpleCommand"].registerCommand(cmdSaid, "said")
    bot.modules["modules.main.simpleCommand"].registerCommand(cmdQuotes, "quotes")
    bot.modules["modules.main.hooks"].addHook("MESSAGE", getPotentialQuotes)


async def cmdQuotes(data, opts=[]):
    """quotes <nick>
    Tells a random <nick>'s quote."""
    global quoteBD

    quoteBD = util.cfg.load("cfg/quote.json")
    if len(quoteBD.keys())>0:
        if len(opts)>0:
            if opts[0][0:2] + opts[0][-1] == "<@>":
                userId = opts[0][2:-1] # "<@[0-9]*>"
                if userId[0] == "!":
                    userId = userId[1:]
                userName = bot.getUserName(userId)
            else:
                userName = opts[0]
        else:
            userNames = list(quoteBD.keys())
            userName = userNames[randint(0, len(userNames)-1)]

        if userName in quoteBD:
            quotes = quoteBD[userName]
            await bot.send(data["tgt"], "« " + quotes[randint(0, len(quotes)-1)] + " » — " + userName)

async def cmdQuote(data, opts=[]):
    """quote <nick> [text to quote if not saved by "said"]
    Saves the last thing <nick> said (or given text)."""
    global quoteBD, lastUserPhrase

    quoteBD = util.cfg.load("cfg/quote.json")
    if len(opts)>0:
        userId = opts[0][2:-1] # "<@[0-9]*>"
        if userId[0] == "!":
            userId = userId[1:]
        userName = bot.getUserName(userId)
        quote = ""
        if len(opts)>1:
                quote = " ".join(opts[1:])
        elif userId in lastUserPhrase:
            quote = lastUserPhrase[userId]
        else:
            await bot.send(data["tgt"], "{} didn't say anything yet !".format(userName))
        if len(quote) > 0:
            if userName in quoteBD:
                quoteBD[userName].append(quote)
            else:
                quoteBD[userName] = [quote, ]
            await bot.send(data["tgt"], "{} has been quoted".format(userName))

    util.cfg.save(quoteBD, "cfg/quote.json")

async def cmdSaid(data, opts=[]):
    """said <nick>
    Shows what <nick> said last."""
    global lastUserPhrase

    if len(opts)>0:
        userId = opts[0][2:-1] # "<@[0-9]*>"
        if userId[0] == "!":
            userId = userId[1:]
        userName = bot.getUserName(userId)
        if userId in lastUserPhrase:
            await bot.send(data["tgt"], "« {} » — {}".format(lastUserPhrase[userId], userName))
        else:
            await bot.send(data["tgt"], "{} didn's say anything yet !".format(userName))

async def getPotentialQuotes(message):
    """Quotes every last phrase of all users"""
    global lastUserPhrase

    lastUserPhrase[message.author.id] = message.content
