#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Regular expression triggers on user messages."""

import re
from random import randint

import util.cfg

bot = None
triggers = {}

def init(botInstance):
    """Inits the msgTrigger module"""
    global bot, triggers

    bot = botInstance
    bot.modules.check("modules.main.simpleCommand")
    bot.modules.check("modules.main.hooks")

    util.cfg.default = triggers
    triggers = util.cfg.load("cfg/triggers.json")

    bot.modules["modules.main.simpleCommand"].registerCommand(cmdDef, "def", ["<@170136309657108480>"])
    bot.modules["modules.main.simpleCommand"].registerCommand(cmdTrg, "trg")
    bot.modules["modules.main.hooks"].addHook('MESSAGE', msgHook)

async def cmdDef(data, opts=[]):
    """Defines a new trigger and it's associated message (or add a message to an existing trigger if it already exists)
    def triggerName expr expression
    def triggerName msg message
    variables: %user=user name"""
    global triggers

    if len(opts)<3:
        await bot.send(data["tgt"], "Sorry ! Not enough parameters. See help !")
        return

    if opts[1] == "expr":
        # Sets the regular expression
        if not triggers.__contains__(opts[0]):
            triggers[opts[0]] = {"expr": " ".join(opts[2:]), "msg":[]}
        else:
            triggers[opts[0]]["expr"] = " ".join(opts[2:])
        await bot.send(data["tgt"], "<@{}> expression set for trigger {}".format(data["user"].id, opts[0]))

    elif opts[1] == "msg":
        # Adds a message
        if not triggers.__contains__(opts[0]):
            triggers[opts[0]] = {"expr": " ", "msg":[]}
        triggers[opts[0]]["msg"].append(" ".join(opts[2:]))
        await bot.send(data["tgt"], "<@{}> message added for trigger {}".format(data["user"].id, opts[0]))
    else:
        await bot.send(data["tgt"], "Sorry ! Subcommand {} not recognized.".format(opts[1]))

    util.cfg.save(triggers, "cfg/triggers.json")

async def cmdTrg(data, opts=[]):
    """List active triggers:
    trg: list all trigger namer
    trg expr name: list expression for trigger name
    trg msg name: list messages for trigger name"""
    from time import sleep

    if len(opts) == 0:
        await bot.send(data["tgt"], "Loaded triggers: " + ", ".join(list(triggers.keys())))

    if len(opts) == 2:
        if opts[0] == "expr" and opts[1] in triggers:
            await bot.send(data["tgt"], "Expression for {} : {}".format(opts[1], triggers[opts[1]]["expr"]))

        elif opts[0] == "msg" and opts[1] in triggers:
            text = "Message(s) for {}".format(opts[1])
            nb = 0
            for message in triggers[opts[1]]["msg"]:
                text += "\n- {}".format(message)
                nb += 1
                if nb % 8 == 0:
                    sleep(1)
            await bot.send(data["tgt"], text)

async def msgHook(message):
    """Hook for the event MESSAGE"""

    user = message.author
    tgt = message.channel
    txt = message.content

    for triggerName in triggers.keys():
        if re.search(triggers[triggerName]["expr"], txt) != None and len(triggers[triggerName]["msg"])>0:
            answer = triggers[triggerName]["msg"][randint(0, len(triggers[triggerName]["msg"])-1)]
            await bot.send(tgt, answer.replace("%user", "<@{}>".format(user.id)))
