#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Markov Chain statistic-based talk :D"""

import data.Markov as Markov
import random
import re

bot = None
splitUserID = re.compile("<@(!?) ([0-9]+) >")
typoComma = re.compile("(\S) , (\S)")
typoApostroph = re.compile("(\S) ' (\S)")
talk = 0

def init(botInstance):
    """Inits the msgTrigger module"""
    global bot
    
    bot = botInstance
    bot.modules.check("modules.main.simpleCommand")

    Markov.initDb()

    bot.modules["modules.main.simpleCommand"].registerCommand(cmdGraph, "graph")
    bot.modules["modules.main.simpleCommand"].registerCommand(cmdRandWalk, "randwalk")
    bot.modules["modules.main.simpleCommand"].registerCommand(cmdTalk, "talk")
    bot.modules["modules.main.simpleCommand"].registerCommand(cmdRandomness, "randomness")
    bot.modules["modules.main.simpleCommand"].registerCommand(cmdFlood, "flood")
    bot.modules["modules.main.simpleCommand"].registerCommand(cmdShut, "shut")
    bot.modules["modules.main.hooks"].addHook('MESSAGE', talkCheck)

async def cmdRandWalk(data, opts=[]):
    """Sets if random walk or not.
    randwalk {on,off}"""

    if len(opts) >= 1 and ["on", "off"].__contains__(opts[0].lower()):
        if opts[0].lower() == "on":
            Markov.cfg["randomWalk"] = True
        else:
            Markov.cfg["randomWalk"] = False
        Markov.saveCfg()

async def cmdGraph(data, opts=[]):
    """Print graph info
    graph: infos on graph"""
    text  = "Word graph contains {} nodes".format(len(Markov.chainDic))
    text += "\nWord dictionary contains {} entries".format(len(Markov.wordDic))
    text += "\nWord graph has a depth of {}".format(Markov.cfg["chainWindowSize"])
    if Markov.cfg["randomWalk"]:
        text += "\nWord graph is walked randomly"
    else:
        text += "\nWord graph is walked randomly, respective to each sequence frequency"
    await bot.send(data["tgt"], text)

async def cmdFlood(data, opts=[]):
    """Lets the bot talk, without taking talkability parameter into account."""
    global talk
    talk = 2

async def cmdTalk(data, opts=[]):
    """Lets the bot talk
    talk [talkability]"""
    global talk
    talk = 1
    if len(opts) > 0:
        Markov.cfg["talkability"] = int(opts[0])
        Markov.saveCfg()

async def cmdRandomness(data, opts=[]):
    """Lets the bot say random things when it has no clue what to answer.
    randomness [0-100]"""
    if len(opts) > 0:
        Markov.cfg["randomness"] = int(opts[0])
        Markov.saveCfg()

async def cmdShut(data, opts=[]):
    """Makes the bot be quiet"""
    global talk
    talk = 0

async def talkCheck(message):
    """Hook for the event PRIVMSG"""

    user = message.author
    tgt = message.channel
    txt = message.content

    # if tgt==bot.cfg["nick"]:
    #     tgt = user

    # Deletes URL links before analysis
    urls = re.findall(' ?https?://\S+', txt)
    if len(urls)>0:
        for url in urls:
            txt = txt.replace(url, "")

    # Text from the bridge => username at the beginning of the string in <>
    usertag = re.match('^<[^>]*> ', txt)
    if usertag!=None:
        txt = txt.replace(usertag.group(), "")

    # Don't analyze commands
    if txt.split()[0][0] == bot.modules["modules.main.simpleCommand"].moduleConf["cmdChar"]:
        return

    if (talk==1 and random.randint(0, 100)<=Markov.cfg["talkability"]) or talk == 2:
        # Last conditions tells the bot to just shut up if he doesn't know what to say next (unless we're in mode 2, then he _must_ talks)
        msg = Markov.computeAnswer(txt)
        if msg!="":
            msg = typoApostroph.sub("\\1'\\2", typoComma.sub("\\1, \\2", splitUserID.sub('<@\\1\\2>', msg)))
            await bot.send(tgt, msg)
    else:
        Markov.analyzeSentence(txt)
