# -*- coding: UTF-8 -*-

"""Logs every user interaction."""

bot = None

import os
import time

def init(botInstance):
    global bot

    bot = botInstance
    bot.modules.check("modules.main.hooks")

    bot.modules["modules.main.hooks"].addHook("MESSAGE", hook_msg)
    
    if not os.path.exists("log"):
        os.mkdir("log")

async def log(target, user, text):
    with open("log/{}.log".format(target), "a") as f:
        f.write("[{ts}] <{u}> {txt}\n".format(ts=time.strftime("%F %T"), u=user, txt=text))

async def hook_msg(message):
    await log(message.channel, message.author.display_name, message.content)
