#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Allows to keep track of actions of users by increasing/decreasing their karma points"""

import re

bot = None
karmaReg = re.compile(r'^\S+[+-]$')
karmaDict = {}

def init(botInstance):
    """Inits the module"""
    global bot

    bot = botInstance

    bot.modules.check("modules.main.simpleCommand")
    bot.modules.check("modules.main.hooks")

    reg = bot.modules["modules.main.simpleCommand"].registerCommand
    hook = bot.modules["modules.main.hooks"].addHook

    reg(cmdKarma, "karma")
    hook('MESSAGE', hookKarmaCount)

async def hookKarmaCount(message):
    """If text looks like "somename" with a + or - at the end −> karma"""
    global karmaDict

    user, target, text = message.author, message.channel, message.content

    if karmaReg.match(text) is not None:
        uname, direction = text[:-1], text[-1]
        if uname not in karmaDict:
            karmaDict[uname] = 0
        karmaDict[uname] += [-1, 1][direction=="+"]
        await bot.send(target, "Karma of {u} is now {p} point(s)".format(u=uname, p=["","+"][karmaDict[uname]>0] + str(karmaDict[uname])))

async def cmdKarma(data, opts=[]):
    """karma <username>
    How many karma points to one have"""
    if len(opts)<1:
        return
    
    uname = opts[0].strip()
    if uname not in karmaDict:
        await bot.send(data["tgt"], "{u} has no karma, sorry :(".format(u=uname))
    else:
        await bot.send(data["tgt"], "{u} has {p} karma points.".format(u=uname, p=["","+"][karmaDict[uname]>0] + str(karmaDict[uname])))
