#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Basic commands
Moved from 01-simpleCommand to avoid having a too big module"""

bot = None

from asyncio import sleep

import random
import re

import util.cfg
import util.exceptions
import util.log

def init(botInstance):
    """Inits the module"""
    global bot

    bot = botInstance
    bot.modules.check("modules.main.simpleCommand")
    reg = bot.modules["modules.main.simpleCommand"].registerCommand

    reg(cmdKill, "kill", ["<@170136309657108480>"])
    reg(cmdAccess, "access", ["<@170136309657108480>"])
    reg(cmdListModules, "modules")
    reg(cmdHelp, "help")
    reg(cmdSay, "say")
    reg(cmdDo, "do")
    reg(cmdMuffin, "muffin")
    reg(cmdHug, ["hug", "hugs", "snugs", "snuggles"])
    reg(cmdDice, "dice")

def checkChannel(data, opts):
    chan = data["tgt"]
    if len(opts)>=1:
        if len(opts)>=2:
            if opts[0][0:2]+opts[0][-1] == "<#>":
                for server in bot.client.servers:
                    for foundChan in server.channels:
                        if foundChan.id == opts[0][2:-1]:
                            chan = foundChan
                            del opts[0]
                            return chan
            elif opts[0][0:1] == "#": #Issue #8
                for server in bot.client.servers:
                    for foundChan in server.channels:
                        if "#" + foundChan.name == opts[0]:
                            chan = foundChan
                            del opts[0]
                            return chan
    return chan

### Bot state ###
async def cmdKill(data, opts=[]):
    """Really stop the bot (console included)"""
    raise util.exceptions.StopException()

### Other commands ###
async def cmdListModules(data, opts=[]):
    """List loaded modules"""

    await bot.send(data["tgt"], "Loaded modules: {}".format(",".join(list(bot.modules))))

async def cmdHelp(data, opts=[]):
    """Display some help.
    help command1 [command2 [command3 […]]]: display command's helptext
    help module module1 [module2 [module3 […]]]: display module's helptext"""

    # Show docstring as help
    if len(opts)==0:
        await bot.send(data["tgt"], "Available commands: {}".format(",".join(list(bot.modules["modules.main.simpleCommand"].registeredCmd.keys()))))
    else:
        # First param = module ? show module docstring
        if opts[0] == "module":
            del opts[0]

            # Show help for each element
            while len(opts)>=1:
                if opts[0] in bot.modules:
                    await bot.send(data["tgt"], "Help for module "  + opts[0] + ":\n" + bot.modules[opts[0]].__doc__)
                else:
                    await bot.send(data["tgt"], "Cannot find help for module '{}'".format(opts[0]))
                del opts[0]
                if len(opts)>0:
                    await sleep(1)
        else:
            # Show help for each element
            while len(opts)>=1:
                if opts[0] in bot.modules["modules.main.simpleCommand"].registeredCmd.keys():
                    await bot.send(data["tgt"], "Help for command " + opts[0] + ":\n" + bot.modules["modules.main.simpleCommand"].registeredCmd[opts[0]].__doc__)
                else:
                    await bot.send(data["tgt"], "Cannot find help for command '{}'".format(opts[0]))
                del opts[0]
                if len(opts)>0:
                    await sleep(1)

##### Says and dos #####
async def cmdSay(data, opts=[]):
    """Say something onto a defined channel (or the current one if not specified).
    say [channel] blah blah"""

    # Checks if there is a valid way to output to some connected channel
    chan = checkChannel(data, opts)
    if chan != None:
        # bot.modules["modules.talk.markov"].Markov.analyzeSentence(" ".join(opts[0:]))
        await bot.send(chan, " ".join(opts[0:]))

async def cmdDo(data, opts=[]):
    """Do an action.
    do [channel] blah blah"""

    # Checks if there is a valid way to output to some connected channel
    chan = checkChannel(data, opts)
    if chan != None:
        await bot.send(chan, "_{}_".format(" ".join(opts[0:])))

# Muffin ! Yummy !
async def cmdMuffin(data, opts=[]):
    """Sends a muffin on someone.
    muffin [channel] nick"""

    from random import randint

    # Checks if there is a valid way to output to some connected channel
    chan = checkChannel(data, opts)
    if chan != None:
        for nick in opts:
            speed = randint(30, 2000)
            await bot.send(chan, "_throws a muffin at {}'s face at {} km/h_".format(nick, speed))
            if speed>=1224:
                await bot.send(chan, "MUFFIN RAINBOOM!!")
            elif speed < 515:
                await bot.send(chan, "I'm too drunk for this anyway.")
            elif speed == 666:
                await bot.send(chan, "Oh noes, the muffin is invoking Lucifer again :(")
            await sleep(0.25)

async def cmdDice(data, opts=[]):
    """Rolls dice(s).
    dice [faces (6)] [quantity (1)]"""
    faces = 6
    q = 1
    if len(opts)>0:
        faces = int(opts[0])
        if len(opts)>1:
            q = int(opts[1])

    if q < 1 or q > bot.cfg["maxDice"]:
        return

    if faces < 2:
        return
    
    await bot.send(data["tgt"], " ".join(["%d" % random.randint(1, faces) for dice in range(q)]))

# Hugs someone if they need it *sqeaks*
async def cmdHug(data, opts=[]):
    """Hugs someone.
    hug [channel] nick
Synonyms: hugs, snugs, snuggles"""

    from random import randint
    chan = checkChannel(data, opts)

    # Checks if there is a valid way to output to some connected channel
    if chan != None:
        data["tgt"] = chan
        for nick in opts:
            await cmdDo(data, ["hugs {} very gently".format(nick), ])
            await sleep(0.25)

##### Command access rules. Wait man, this is serious shit done down there. Don't touch. #####
async def cmdAccess(data, opts=[]):
    """Manages access to commands
    access command: list user able to run the command (if the command is restricted)
    access command del number: del the numberth access list element for command
    access command add rule [rule2 […]]: add access rule to command
    access command edit ruleNumber ruleContent"""

    if len(opts) == 1:
        # Check rules for a defined command
        if not opts[0] in bot.modules["modules.main.simpleCommand"].moduleConf["access"]:
            await bot.send(data["tgt"], "This command is not restricted or does not exist.")
        else:
            await bot.send(data["tgt"], "Acces is granted to this command to :")

            # Command found, list the rules
            for num, rule in enumerate(bot.modules["modules.main.simpleCommand"].moduleConf["access"][opts[0]]):
                await bot.send(data["tgt"], "{}: '{}'".format(num, rule))

    elif len(opts)>=3:
        # Delete a rule
        if opts[1] == "del":
            if int(opts[2]) >= len(bot.modules["modules.main.simpleCommand"].moduleConf["access"][opts[0]]):
                await bot.send(data["tgt"], "This rule number is invalid for this command.")
            else:
                del bot.modules["modules.main.simpleCommand"].moduleConf["access"][opts[0]][int(opts[2])]
                await bot.send(data["tgt"], "Rule deleted.")

            if len(bot.modules["modules.main.simpleCommand"].moduleConf["access"][opts[0]]) == 0:
                await bot.send(data["tgt"], "No rule left, command is now unrestricted.")
                del bot.modules["modules.main.simpleCommand"].moduleConf["access"][opts[0]]

        # Add rules
        if opts[1] == "add":
            if not opts[0] in bot.modules["modules.main.simpleCommand"].moduleConf["access"]:
                await bot.send(data["tgt"],"Command was not restricted, registering it into the access list.")
                bot.modules["modules.main.simpleCommand"].moduleConf["access"][opts[0]] = []

            for rule in opts[2:]:
                bot.modules["modules.main.simpleCommand"].moduleConf["access"][opts[0]].append(rule)
                await bot.send(data["tgt"],"Rule(s) added.")

        # Edit a rule
        if opts[1] == "edit" and len(opts) == 4:
            if not opts[0] in bot.modules["modules.main.simpleCommand"].moduleConf["access"]:
                await bot.send(data["tgt"],"This command is not restricted or does not exist.")
            else:
                if int(opts[2]) >= len(bot.modules["modules.main.simpleCommand"].moduleConf["access"][opts[0]]):
                    await bot.send(data["tgt"],"This rule number is invalid for this command.")
                else:
                    bot.modules["modules.main.simpleCommand"].moduleConf["access"][opts[0]][int(opts[2])] = opts[3]
                    await bot.send(data["tgt"],"Access rule edited.")

        # Write everything back to the file
        util.cfg.save(bot.modules["modules.main.simpleCommand"].moduleConf, "cfg/commands.json")
