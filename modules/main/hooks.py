# -*- coding: UTF-8 -*-

"""Main hooks module"""

bot = None

import time

def init(botInstance):
    global bot

    bot = botInstance

def addHook(event, func):
    global bot
    print('> HOOKING {nm} into {e} event'.format(nm=func.__module__+'.'+func.__name__, e=event))
    if bot.hooks.__contains__(event):
        bot.hooks[event].append(func)
    else:
        bot.hooks[event] = [func, ]

##### Main HOOKS #####
