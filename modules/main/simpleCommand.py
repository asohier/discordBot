#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Get commands from irc

init(data): init the module
registerCommand(command, name, accessRules): register command under name, with optionnal accessRules list
recvCommand(evt): run command detection against the current event line (space-splitted)"""

import re

import util.cfg
import util.log

moduleConf = {
        "cmdChar": "!",
        "access": {
            "kill": ["<@170136309657108480>"],
        }
}
registeredCmd = {}
bot = None

##### INIT the module (needed) #####
def init(botInstance):
    global bot, moduleConf

    bot = botInstance
    bot.modules.check("modules.main.hooks")

    util.cfg.default = moduleConf
    moduleConf = util.cfg.load("cfg/commands.json")
    bot.modules["modules.main.hooks"].addHook('MESSAGE', recvCommand)

##### register commands #####
def registerCommand(command, names, accessRules=[]):
    """Register a command."""
    global registeredCmd, moduleConf
    if type(names) == type(str()):
        names = [names, ]
    for name in names:
        registeredCmd[name] = command

        print("Registered command {}".format(name))

        # Have some access rules ? register them and write them for future use (if we don't already have them).
        if len(accessRules)>0 and not moduleConf["access"].__contains__(name):
            moduleConf["access"][name] = accessRules
            util.cfg.save(moduleConf, "cfg/commands.json")

##### hook functions #####
async def recvCommand(message):
    """Receive commands from IRC and dispatch them"""
    global bot
    
    user = message.author
    tgt = message.channel
    cmd = message.content.split()

    # If user talked to us in private message, answers him the same way
    # if tgt==bot.cfg["nick"]:
    #     tgt=user

    # Strip from empty items on the start (mainly because of spaces)
    while len(cmd)>1 and len(cmd[0])<1:
        del cmd[0]

    # No command char ? meh. Certainly a dumb user who is just talking.
    # Nevermind. *going back to sleep*
    # Also, don't execute our own commands, if any.
    # Just in case.
    if len(cmd) != 0 and len(cmd[0]) != 0 and cmd[0][0]==moduleConf["cmdChar"] and user.id!=bot.client.user.id:
        # Check execution privileges
        print("> It's a command.")
        runnable = True
        if cmd[0][1:] in moduleConf["access"]:
            runnable = False

            # 'Normal' channel message, so verify carefully who is asking what.
            for pattern in moduleConf["access"][cmd[0][1:]]:
                if re.search(pattern, "<@{}>".format(user.id)) != None:
                    runnable = True
        if not runnable:
            print("{} ({}) have no right to run {}".format(user.name, user.id, cmd[0][1:]))
            await bot.send(message.channel, "Sorry <@{}>, I'm afraid I can't let you do that.".format(user.id))
        else:
            print("> This command can be run.")
            # Run command, if it was registered
            if registeredCmd.__contains__(cmd[0][1:]):
                print("> Running {}".format(cmd[0]))
                if len(cmd)>1:
                    await registeredCmd[cmd[0][1:]]({"source":message.channel, "user":user, "tgt": tgt}, cmd[1:])
                else:
                    await registeredCmd[cmd[0][1:]]({"source":message.channel, "user":user, "tgt": tgt})
            else:
                print("> {}: command not found".format(cmd[0]))
                await bot.send(tgt, "Sorry <@{}>, I can't find {} in my modules.".format(user.id, cmd[0]))
