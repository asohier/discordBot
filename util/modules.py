#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# vim: foldlevel=0
import os
import importlib

import util.cfg

class Modules:
    """Live modules management engine"""
    # Internal modifs to allow Modules[modpath] addressing scheme
    def __init__(self, botInstance):
        self.Data = {}
        self.bot = botInstance
    def __getitem__(self, item):
        return self.Data[item]
    def __setitem__(self, item, value):
        self.Data[item] = value
    def __delitem__(self, item):
        del self.Data[item]
    def __len__(self):
        return len(self.Data)
    def __contains__(self, item):
        return self.Data.__contains__(item)
    def __iter__(self):
        return iter(self.Data)

    # String representation
    def __repr__(self):
        return '  - ' + '\n  - '.join(['{name} ({repr})'.format(name=i, repr=self.Data[i].__repr__()) for i in self.Data.keys()])

    def live(self):
        """Lists every loaded module"""
        return self.Data.keys()
    def load(self, modpath, force=False):
        """Loads a module.
        modpath is in the python namespace form, eg if the file to be found is under util/cfg.py, it must be specified as util.cfg"""
        if self.Data.__contains__(modpath):
            if not force:
                return
            self.unload(modpath)
        if os.path.exists(modpath.replace(".", "/") + ".py"):
            print("{i} found, loading.".format(i=modpath))
            self[modpath] = importlib.import_module(modpath)
            self[modpath].init(self.bot)
    def check(self, modpath):
        """Checks that a module is loaded."""
        if not self.Data.__contains__(modpath):
            print("> {i} required, loading in advance".format(i=modpath))
            self.load(modpath)
    def unload(self, modpath):
        """Unloads a module.
        modpath: see load"""
        if self.Data.__contains__(modpath):
            del self.Data[modpath]
    def loadAll(self, basedir):
        """Load all .py files that can be found under basedir."""
        for root, dirs, files in os.walk(basedir):
            dirs.sort()
            files.sort()
            for item in files:
                if item[-3:] == ".py":
                    mod = root.replace("/", ".") + "." + item[:-3]
                    self.load(mod)
